import config from '../common/config'
class Tool {

    getscreenwidth=()=>
    {
        return document.body.clientWidth;
    }

    getdetailurl=(relateurl)=>{
        return config.sitedetailurl+"/#"+relateurl;
    }

    getmainurl=(relateurl)=>{
        return config.sitemainurl+"/#"+relateurl;
    }

    setparam=(page,key,value)=>{
        key = page+"_"+key;
        localStorage.setItem(key,value);
    }

    getparam=(page,key,defaultvalue)=>{
        key = page+"_"+key;
        let v = localStorage.getItem(key);
        console.log("local存储"+v);
        if(v===undefined||v===null||v==='undefined')
        {
            return defaultvalue;
        }
        return v;
    }
}

const tool = new Tool();

export default tool;