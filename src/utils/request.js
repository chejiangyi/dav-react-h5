import fetch from 'dva/fetch';
import config from '../common/config';

function parseJSON(response) {
  let json = response.json();
  return json;
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

function checkProcessError(response)
{
  if(response.code!==1)//自定义的协议解析
  {
    const error = new Error(response.msg);
    error.response = response;
    throw error;
  }
  return response;
}

function getForm(obj)
{
  let formData = new URLSearchParams(); 
  Object.keys(obj).forEach((c)=>{
    formData.append(c,obj[c]);
  });
  //console.log(formData);
  return formData;
}


/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, options) {
  return fetch(url, options)
    .then(checkStatus)
    .then(parseJSON)
    .then(data => ({ data }))
    .catch(err => ({ err }));
}

export function requesttext(url, options,datacallback,errorcallback,staetcallback) {
  if(staetcallback!=null)
  staetcallback(true);

return fetch(url, options)
  .then(checkStatus)
  .then(parseJSON)
  .then(checkProcessError)
  .then(data => {
    if(staetcallback!=null)
      staetcallback(false);
    if(datacallback!=null&&data!==undefined)
      datacallback(data);
  })
  .catch(err => {
    if(staetcallback!=null)
    staetcallback(false);
    if(errorcallback!=null)
      errorcallback(err);
  });
}

export function requestbase(type,method,url,params,datacallback,errorcallback,staetcallback) {
  let submitdata = params;let headers={};
  if(type==="json")
  {
    submitdata=JSON.stringify(params);
  }
  else if(type==='form')
  {
    submitdata=getForm(params);
    headers={"Content-Type": "application/x-www-form-urlencoded"};
  }
  var optionsdata = {
    method: method,
    headers:headers,
    body: submitdata,
  }
  // const options = {...optionsdata };
  // options.headers = {
  //   Accept: 'application/json',
  //   'Content-Type': 'application/json; charset=utf-8',
  //   ...options.headers,
  // };
  // options.body = JSON.stringify(options.body);
  if(staetcallback!=null)
    staetcallback(true);
  
  return fetch(config.siteurl+url, optionsdata)
    .then(checkStatus)
    .then(parseJSON)
    .then(checkProcessError)
    .then(data => {
      if(staetcallback!=null)
        staetcallback(false);
      if(datacallback!=null&&data!==undefined)
        datacallback(data);
    })
    .catch(err => {
      if(staetcallback!=null)
      staetcallback(false);
      if(errorcallback!=null)
        errorcallback(err);
    });
}



export function post(url, params,datacallback,errorcallback,staetcallback) {
  return requestbase("form","POST",url,params,datacallback,errorcallback,staetcallback);
}

export function get(url, params,datacallback,errorcallback,staetcallback) {
  return requestbase("form","GET",url,params,datacallback,errorcallback,staetcallback);
}

