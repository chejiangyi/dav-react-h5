import React from 'react';
import { Router, Route, Switch } from 'dva/router';
import {getRouterData} from './common/drouter';

function RouterConfig({ history,app }) {
  const routerData = getRouterData(app);
  const HomeLayout = routerData['/'].component;

  return (
    <Router history={history}>
      <Switch>
        <Route path="/home" exact component={routerData['/home'].component} />
        <Route path="/article" exact component={routerData['/article'].component} />
        <Route path="/picture" exact component={routerData['/picture'].component} />
        <Route path="/video" exact component={routerData['/video'].component} />
        <Route path="/articledetail/:id" exact component={routerData['/articledetail'].component} />
        <Route path="/videodetail/:id" exact component={routerData['/videodetail'].component} />
        <Route path="/picturedetail/:id" exact component={routerData['/picturedetail'].component} />  
        <Route path="/about/" exact component={routerData['/about'].component} />
        <Route path="/" exact component={HomeLayout} />
      </Switch>
    </Router>
  );
}

export default RouterConfig;
