import React, {Component} from 'react';
import Layout from '../components/Layout';
import {Result} from 'antd-mobile';
import { connect } from 'dva';
import config from '../common/config';
import site from '../assets/site.jpg';



@connect((loading)=>({
}))
export default class About extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            category:[],
            datalist:[],
            selectcategory:0,
            pageCurrent: 1,
            pageSize: 15,
        };
    }
    //组件加载
    componentDidMount() {
        document.title="关于-"+config.sitename;
    }

    //组件卸载
    componentWillUnmount() {

    }
    //属性改变
    componentWillReceiveProps(nextProps) {
    }


    render() {
        return (
            <Layout selectedTab={"About"} {...this.props}>
                <Result
                    img={<img src={site} className="" alt="" />}
                    title={config.sitename}
                    message={<div>竭诚为你服务,您的满意是我们前进的动力</div>}
                />
            </Layout>
        );
    }
}
