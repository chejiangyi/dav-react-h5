import React, {Component} from 'react';
import {Link} from 'dva/router';
import Layout from '../components/Layout';
import {WhiteSpace,Flex,ActivityIndicator,Toast,Tabs,List,Text} from 'antd-mobile';
import { post } from '../utils/request';
import { connect } from 'dva';
import {routerRedux} from 'dva/router';
import SyList from '../components/SyList';
import config from '../common/config';
import st from '../site.less';
import tool from '../utils/tool';


@connect((loading)=>({
}))
export default class Pictrue extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            category:[],
            datalist:[],
            selectcategory:0,
            pageCurrent: 1,
            pageSize: 15,
        };
    }
    //组件加载
    componentDidMount() {
        document.title="精选图吧-"+config.sitename;
        post("/openapi/getcategory/",{
            category:"pic"
        },(r)=>{
            let cs=[];
            r.data.forEach(c => {
                cs.push({title:c.name,data:c});
            });
            this.setState({
                category:cs,
                selectcategory:cs[0].data.value
            },()=>{
                this.loadData();
            });
           
           
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    //组件卸载
    componentWillUnmount() {

    }
    //属性改变
    componentWillReceiveProps(nextProps) {
    }

    loadData=()=> {
        // console.log({
        //     category:2,
        //     subcategory:this.state.selectcategory,
        //     pageindex:this.state.pageCurrent,
        //     pagesize:this.state.pageSize,
        // });
        post("/openapi/getInfoByCategory/",{
            category:2,
            subcategory:this.state.selectcategory,
            pageindex:this.state.pageCurrent,
            pagesize:this.state.pageSize,
        },(r)=>{
            this.setState({
                datalist: this.state.datalist.concat(r.data)
            });
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    tabClick=(tab,index)=>{
        this.setState({
            datalist:[],
            selectcategory:tab.data.value,
            pageCurrent:1
        },()=>{ this.loadData();});
       
    };

    listItemClick=(id)=>{
        this.props.dispatch(routerRedux.push({
            pathname: '/picturedetail/'+id
        }));
    }

    renderRow = (rowData, sectionID, rowID) => {
        let item = rowData;
        //onClick={()=>this.listItemClick(item.id)}
        return (
            <a href={tool.getdetailurl('/picturedetail/'+item.id)} style={{color:"black"}}  target="_blank">
            <List.Item key={item.id}  >
                 <Flex>
                 <Text className={st.title} style={{width:tool.getscreenwidth()-30}}>{item.title}</Text>
                </Flex>
                <Flex>
                    {item.imgurl.split(',').map((a,i)=>(<img id={"img_"+item.id+"_"+i} style={{width:'100px'}} alt={""} src={a}  onLoad={() => {
                                var imgid = "img_"+item.id+"_"+i;
                                document.getElementById(imgid).style.height='auto';
                            }}/>))}
                </Flex>
             </List.Item>
             </a>
        );
    };

    onEndReached=()=> {
        this.setState({
            pageCurrent: this.state.pageCurrent + 1
        },()=>{this.loadData();});
    }

    render() {
        return (
            <Layout selectedTab={"Picture"} {...this.props}>
                <Tabs tabs={this.state.category} onTabClick={this.tabClick}>
                </Tabs>
                <div style={{
                    width: '100%',
                    overflow: 'auto',
                    top: '6.5rem',
                    position: 'absolute',
                    //zIndex: 10,
                    bottom: '3rem',
                    }}>
                <SyList
                        data={this.state.datalist}
                        renderRow={this.renderRow}
                        className="am-list"
                        pageSize={this.state.pageSize}
                        pageCurrent={this.state.pageCurrent}
                        onEndReached={this.onEndReached}
                        onEndReachedThreshold={5}
                    />
                    </div>
                <ActivityIndicator
                    toast
                    size="large"
                    text="数据加载中..."
                    animating={this.state.loading}
                />
            </Layout>
        );
    }
}
