import React, {Component} from 'react';
import {Link} from 'dva/router';
import Layout from '../components/Layout';
import config from '../common/config';
import {List,WhiteSpace,Flex,ActivityIndicator,Toast,Tabs,Text} from 'antd-mobile';
import { post } from '../utils/request';
import SyList from '../components/SyList';
import { connect } from 'dva';
import st from '../site.less';
import tool from '../utils/tool';


@connect((loading)=>({
}))
export default class VideoDetail extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            id: props.match.params.id,
            title:"",
            urls:"",
            imgurl:"",
        };
    }
    //组件加载
    componentDidMount() {
        post("/openapi/getInfo/",{
            id:this.state.id
        },(r)=>{
            document.title=r.data.title +"-视频-"+config.sitename;
            this.setState({
                title:r.data.title,
                urls:r.data.urls,
                imgurl:r.data.imgurl,
            });
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    //组件卸载
    componentWillUnmount() {

    }
    //属性改变
    componentWillReceiveProps(nextProps) {
    }

    openVideo(url)
    {
        window.location.href=url;
    }

    render() {
        return (
            <Layout Back selectedTab={"Video"} {...this.props}>
                
                <div style={{
                    width: '100%',
                    overflow: 'auto',
                    top: '4rem',
                    position: 'absolute',
                    //zIndex: 10,
                    bottom: '3rem',
                    }}>
                <Flex wrap="wrap" style={{margin:"3px", justifyContent:"center"}}>
                    {this.state.title}
                </Flex>
                <Flex className={st.detail} style={{margin:"3px", justifyContent:"center"}}>
                    {/* <a href={this.state.urls} style={{background:"url("+require('../assets/imgs/play.png')+") no-repeat center center top",zIndex:"999"}}  target="_blank">
                    <img src={this.state.imgurl} alt="" style={{width:tool.getscreenwidth()}}></img>
                    </a> */}
                    <a href={this.state.urls}  target="_blank" style={{position:"relative"}}>
                    <img src={this.state.imgurl} alt="" style={{width:tool.getscreenwidth()}}>
                    </img>
                    <img src={require('../assets/imgs/play.png')} alt="" style={{position:"absolute", bottom:"45%",left:"45%"}}></img>
                    </a>
                     {/* <iframe title="aaa" src="https://yaoshe24.com/embed/21782"></iframe> */}
                    {/* <Player><source src={this.state.urls} /></Player> */}
                    {/* <img src={this.state.imgurl} alt="" style={{width:tool.getscreenwidth()}}></img> */}
                </Flex>
                </div>
                <ActivityIndicator
                    toast
                    size="large"
                    text="数据加载中..."
                    animating={this.state.loading}
                />
            </Layout>
        );
    }
}
