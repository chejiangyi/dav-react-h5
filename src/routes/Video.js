import React, {Component} from 'react';
import {Link} from 'dva/router';
import Layout from '../components/Layout';
import {WhiteSpace,Flex,ActivityIndicator,Toast,Tabs,List,Text} from 'antd-mobile';
import { post } from '../utils/request';
import { connect } from 'dva';
import SyList from '../components/SyList';
import {routerRedux} from 'dva/router';
import config from '../common/config';
import st from '../site.less';
import tool from '../utils/tool';

@connect((loading)=>({
}))
export default class Video extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            category:[],
            datalist:[],
            selectcategory:0,
            pageCurrent: 1,
            pageSize: 15,
        };
    }
    //组件加载
    componentDidMount() {
        post("/openapi/getcategory/",{
            category:"video"
        },(r)=>{
            let cs=[];
            r.data.forEach(c => {
                cs.push({title:c.name,data:c});
            });
            this.setState({
                category:cs,
                selectcategory:cs[0].data.value
            },()=>{
                this.loadData();
            });
           
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    //组件卸载
    componentWillUnmount() {

    }
    //属性改变
    componentWillReceiveProps(nextProps) {
    }

    loadData=()=> {
        document.title="精选视频-"+config.sitename;
        post("/openapi/getInfoByCategory/",{
            category:1,
            subcategory:this.state.selectcategory,
            pageindex:this.state.pageCurrent,
            pagesize:this.state.pageSize,
        },(r)=>{
            this.setState({
                datalist: this.state.datalist.concat(r.data)
            });
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    tabClick=(tab,index)=>{
        this.setState({
            datalist:[],
            selectcategory:tab.data.value,
            pageCurrent:1
        },()=>{
            this.loadData();
        });
    };

    listItemClick=(id)=>{
        this.props.dispatch(routerRedux.push({
            pathname: '/videodetail/'+id
        }));
    }

    renderRow = (rowData, sectionID, rowID) => {
        let item = rowData;
        //onClick={()=>this.listItemClick(item.id)}
        return (
            <a href={tool.getdetailurl('/videodetail/'+item.id)} style={{color:"black"}} target="_blank">
            <List.Item key={item.id}  >
                 <Flex>
                 <Text className={st.title} style={{width:tool.getscreenwidth()-30}}>{item.title}</Text>
                </Flex>
                <Flex>
                   <img id={"img_"+item.id} style={{width:tool.getscreenwidth()-30}} alt={item.title} src={item.imgurl}   onLoad={() => {
                                var imgid = "img_"+item.id;
                                document.getElementById(imgid).style.height='auto';
                            }}/>
                </Flex>
             </List.Item>
             </a>
        );
    };

    onEndReached=()=> {
        this.setState({
            pageCurrent: this.state.pageCurrent + 1
        }, () => {
            this.loadData();
        });
    }

    render() {
        return (
            <Layout selectedTab={"Video"} {...this.props}>
                <Tabs tabs={this.state.category} onTabClick={this.tabClick}>
                </Tabs>
                <div style={{
                    width: '100%',
                    overflow: 'auto',
                    top: '7rem',
                    position: 'absolute',
                    bottom: '3rem',
                    }}>
                <SyList
                        data={this.state.datalist}
                        renderRow={this.renderRow}
                        className="am-list"
                        pageSize={this.state.pageSize}
                        pageCurrent={this.state.pageCurrent}
                        onEndReached={this.onEndReached}
                        onEndReachedThreshold={5}
                    />
                    </div>
                <ActivityIndicator
                    toast
                    size="large"
                    text="数据加载中..."
                    animating={this.state.loading}
                />
            </Layout>
        );
    }
}
