import React, {Component} from 'react';
import {Link,routerRedux} from 'dva/router';
import Layout from '../components/Layout';
import {WhiteSpace,Flex,ActivityIndicator,List,Toast,Carousel,Text} from 'antd-mobile';
import { post } from '../utils/request';
import SyList from '../components/SyList';
import { connect } from 'dva';
import config from '../common/config';
import st from '../site.less';
import tool from '../utils/tool';
import ReactDOM from 'react-dom';


@connect((loading)=>({
}))

export default class Home extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            videodatalist:[],
            picturedatalist:[],
            articledatalist:[],
            imgHeight:'auto',
        };
    }
    //组件加载
    componentDidMount() {
        document.title=config.sitename+"-欢迎你";
        let videodata=[];let picdata=[];let articledata=[];
        this.dataload(1,0,(r)=>{
            this.setState({videodatalist:r.data},()=>{
                this.resize();
            });
        });
        this.dataload(2,0,(r)=>{
            this.setState({picturedatalist:r.data},()=>{
                this.resize();
            });
        });
        this.dataload(3,0,(r)=>{
            this.setState({articledatalist:r.data},()=>{
                this.resize();
            });
        });
    }

    resize=()=>{
        let node = ReactDOM.findDOMNode(this.videolist);
        node.style.height = node.children[0].offsetHeight+10+'px';
        node = ReactDOM.findDOMNode(this.picturelist);
        node.style.height = node.children[0].offsetHeight+10+'px';
        node = ReactDOM.findDOMNode(this.articlelist);
        node.style.height = node.children[0].offsetHeight+10+'px';
    }
    

    //组件卸载
    componentWillUnmount() {
    }
    //属性改变
    componentWillReceiveProps(nextProps) {
    }

    dataload =(category,subcategory,callback)=>{
        post("/openapi/getInfoByCategory/",{
            category:category,
            subcategory:subcategory,
            pageindex:1,
            pagesize:10,
        },(r)=>{
            callback(r);
        },(e)=>{
            //Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    videorenderRow = (rowData, sectionID, rowID) => {
        let item = rowData;
        return (
            <a href={tool.getdetailurl('/videodetail/'+item.id)} style={{color:"black"}}  target="_blank">
            <List.Item>
                 <Flex key={"video_"+rowData.id}>
                 <Text className={st.title} style={{width:tool.getscreenwidth()-30}}>{item.title}</Text>
                 </Flex>
            </List.Item>
            </a>
        );
    }

    picturerenderRow = (rowData, sectionID, rowID) => {
        let item = rowData;
        return (
            <a href={tool.getdetailurl('/picturedetail/'+item.id)} style={{color:"black"}}  target="_blank">
             <List.Item key={"pictrue_"+rowData.id}> 
                 <Flex><Text className={st.title} style={{width:tool.getscreenwidth()-30}}>{item.title}</Text>
             </Flex>
            </List.Item>
            </a>
        );
    };

    articlerenderRow = (rowData, sectionID, rowID) => {
        let item = rowData;
        return (
            <a href={tool.getdetailurl('/articledetail/'+item.id)} style={{color:"black"}}  target="_blank">
             <List.Item key={"article_"+rowData.id}> 
                 <Flex><Text className={st.title} style={{width:tool.getscreenwidth()-30}}>{item.title}</Text>
             </Flex>
            </List.Item>
            </a>
        );
    };

    render() {
        return (
            <Layout selectedTab={"Home"} {...this.props}>
               <div className={st.view}>
                <Carousel
                    autoplay={false}
                    infinite
                     beforeChange={(from, to) => console.log(`slide from ${from} to ${to}`)}
                     afterChange={index => console.log('slide to', index)}
                    >
                    {config.siteimgurls.map(val => (
                        <a
                        key={val}
                        href="javasrcipt:"
                        style={{ display: 'inline-block', width: '100%', height: this.state.imgHeight }}
                        >
                        <img
                            src={val}
                            alt=""
                            style={{ width: '100%', verticalAlign: 'top' }}
                            onLoad={() => {
                                window.dispatchEvent(new Event('resize'));
                                this.setState({ imgHeight: 'auto' });
                            }}
                        />
                        </a>
                    ))}
                 </Carousel>
                    <Flex className={st.lan}>热门视频</Flex>
                    <SyList
                        ref={(ref)=>{this.videolist=ref}}
                        data={this.state.videodatalist}
                        renderRow={this.videorenderRow}
                        className="am-list"
                        overflow="hidden"
                        //height={this.state.videoheight}
                    />
                    <WhiteSpace/>
                    <Flex className={st.lan}>热门图片</Flex>
                    <SyList
                        ref={(ref)=>{this.picturelist=ref}}
                        data={this.state.picturedatalist}
                        renderRow={this.picturerenderRow}
                        className="am-list"
                        overflow="hidden"
                        //height={this.state.pictrueheight}
                    />
                    <WhiteSpace/>
                    <Flex className={st.lan}>热门文章</Flex>
                    <SyList
                        ref={(ref)=>{this.articlelist=ref}}
                        data={this.state.articledatalist}
                        renderRow={this.articlerenderRow}
                        className="am-list"
                        overflow="hidden"
                        //height={this.state.articleheight}
                    />
                </div>
                
                <ActivityIndicator
                    toast
                    size="large"
                    text="数据加载中..."
                    animating={this.state.loading}
                />
            </Layout>
            
        );
    }
}
