import React, {Component} from 'react';
import {Link} from 'dva/router';
import Layout from '../components/Layout';
import config from '../common/config';
import {List,WhiteSpace,Flex,ActivityIndicator,Toast,Tabs,Text} from 'antd-mobile';
import { post, requesttext } from '../utils/request';
import SyList from '../components/SyList';
import { connect } from 'dva';
import ReactDOM from 'react-dom';


@connect((loading)=>({
}))
export default class ArticleDetail extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            id: props.match.params.id,
            title:"",
            detail:""
        };
    }
    //组件加载
    componentDidMount() {
        
        post("/openapi/getInfo/",{
            id:this.state.id
        },(r)=>{
            document.title=r.data.title +"-文章-"+config.sitename;
            // requesttext(config.fileurl+r.data.urls,{
            //     method:"GET"
            // },(r2)=>{
            //     this.setState({
            //         title:r.data.title,
            //         detail:r2
            //     },()=>{
            //        let detailnode= ReactDOM.findDOMNode(this.detailref);
            //        let titlenode = ReactDOM.findDOMNode(this.titleref);
            //        detailnode.style.top=titlenode.offsetHeight+titlenode.offsetTop + 10 +"px";
            //     });
            // },(e2)=>{
            //     Toast.fail(e2.message,3);
            // },(s2)=>{
            //     this.setState({loading:s2});
            // });

            this.setState({
                title:r.data.title,
                detail:r.data.detail
            },()=>{
               let detailnode= ReactDOM.findDOMNode(this.detailref);
               let titlenode = ReactDOM.findDOMNode(this.titleref);
               detailnode.style.top=titlenode.offsetHeight+titlenode.offsetTop + 10 +"px";
            });
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    //组件卸载
    componentWillUnmount() {

    }
    //属性改变
    componentWillReceiveProps(nextProps) {
    }

    render() {
        return (
            <Layout Back selectedTab={"Article"} {...this.props}>
                <Flex ref={(ref)=>{this.titleref=ref}} wrap="wrap" style={{margin:"3px",justifyContent:"center"}}>
                    {this.state.title}
                </Flex>
                <div ref={(ref)=>{this.detailref=ref}} style={{
                    width: '100%',
                    overflow: 'auto',
                    top: '6rem',
                    position: 'absolute',
                    //zIndex: 10,
                    bottom: '3rem',
                    }}>
                <Flex style={{margin:"7px",marginTop:"0px"}}>
                    <Text dangerouslySetInnerHTML={{__html:this.state.detail}}></Text>
                </Flex>
                </div>
                <ActivityIndicator
                    toast
                    size="large"
                    text="数据加载中..."
                    animating={this.state.loading}
                />
            </Layout>
        );
    }
}
