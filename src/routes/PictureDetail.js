import React, {Component} from 'react';
import {Link} from 'dva/router';
import Layout from '../components/Layout';
import config from '../common/config';
import {List,WhiteSpace,Flex,ActivityIndicator,Toast,Tabs,Text} from 'antd-mobile';
import { post } from '../utils/request';
import SyList from '../components/SyList';
import { connect } from 'dva';
import st from '../site.less';
import tool from '../utils/tool';
import Zmage from 'react-zmage';


@connect((loading)=>({
}))
export default class PictureDetail extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            id: props.match.params.id,
            title:"",
            urls:""
        };
    }
    //组件加载
    componentDidMount() {
        post("/openapi/getInfo/",{
            id:this.state.id
        },(r)=>{
            document.title=r.data.title +"-图片-"+config.sitename;
            this.setState({
                title:r.data.title,
                urls:r.data.urls
            });
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    //组件卸载
    componentWillUnmount() {

    }
    //属性改变
    componentWillReceiveProps(nextProps) {
    }

    render() {
        return (
            <Layout Back selectedTab={"Picture"} {...this.props}>
                <Flex wrap="wrap" style={{margin:"3px",justifyContent:"center"}}>
                    {this.state.title}
                </Flex>
                <Flex style={{marginLeft:"5px"}}>
                <div style={{
                    width: '100%',
                    overflow: 'auto',
                    top: '6rem',
                    position: 'absolute',
                    //zIndex: 10,
                    bottom: '3rem',
                    }}>
                    
                    {this.state.urls.split(',').map((a,i)=>(<div><Zmage  id={"img_"+this.state.id+"_"+i} style={{width:tool.getscreenwidth()-10}} alt={""} src={a}  onLoad={() => {
                                //window.dispatchEvent(new Event('resize'));
                                var imgid = "img_"+this.state.id+"_"+i;
                                document.getElementById(imgid).style.height='100%';
                                document.body.height="100%";
                            }}/><br/></div>))}
                </div>
                </Flex>
                <ActivityIndicator
                    toast
                    size="large"
                    text="数据加载中..."
                    animating={this.state.loading}
                />
            </Layout>
        );
    }
}
