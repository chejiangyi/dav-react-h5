import React, {Component} from 'react';
import {Link} from 'dva/router';
import Layout from '../components/Layout';
import config from '../common/config';
import {List,WhiteSpace,Flex,ActivityIndicator,Toast,Tabs,Text} from 'antd-mobile';
import { post } from '../utils/request';
import SyList from '../components/SyList';
import { connect } from 'dva';
import {routerRedux} from 'dva/router';
import st from '../site.less';
import tool from '../utils/tool';

@connect((loading)=>({
}))
export default class Article extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            category:[],
            datalist:[],
            selectcategory:0,
            pageCurrent:1,
            pageSize: 20,
        };
    }
    //组件加载
    componentDidMount() {
        document.title="精选文章-"+config.sitename;
        post("/openapi/getcategory/",{
            category:"article"
        },(r)=>{
            let cs=[];
            r.data.forEach(c => {
                cs.push({title:c.name,data:c});
            });
            this.state.category=cs;
            this.state.selectcategory = (this.state.selectcategory==0?cs[0].data.value:this.state.selectcategory);
            this.loadData();
            // this.setState({
            //     category:cs,
            //     selectcategory:(this.state.selectcategory==0?cs[0].data.value:this.state.selectcategory),
            // },()=>{
            //     this.loadData();
            // });
           
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    //组件卸载
    componentWillUnmount() {

    }
    //属性改变
    componentWillReceiveProps(nextProps) {
    }

    loadData=()=> {
        console.log("优化测试");
        console.log({
            category:3,
            subcategory:this.state.selectcategory,
            pageindex:this.state.pageCurrent,
            pagesize:this.state.pageSize,
        });
        post("/openapi/getInfoByCategory/",{
            category:3,
            subcategory:this.state.selectcategory,
            pageindex:this.state.pageCurrent,
            pagesize:this.state.pageSize,
        },(r)=>{
           
            this.setState({
                datalist: this.state.datalist.concat(r.data)
            });
            // tool.setparam(page,"selectcategory",this.state.selectcategory);
            // tool.setparam(page,"pageCurrent",this.state.pageCurrent);
            // tool.setparam(page,"pageSize",this.state.pageSize);
            //console.log(this.state.datalist);
        },(e)=>{
            Toast.fail(e.message,3);
        },(s)=>{
            this.setState({loading:s});
        });
    }

    tabClick=(tab,index)=>{
        //console.log("选择"+tab.data.value);
        this.state.datalist = [];
        this.state.selectcategory=tab.data.value;
        this.state.pageCurrent = 1;
        this.loadData();
        // this.setState({
        //     datalist:[],
        //     selectcategory:tab.data.value,
        //     pageCurrent:1
        // },()=>{
        //     this.loadData();
        // });
    };

    listItemClick=(id)=>{
        this.props.dispatch(routerRedux.push({
            pathname: '/articledetail/'+id
        }));
    }

    renderRow = (rowData, sectionID, rowID) => {
      
        let item = rowData;
        //console.log(item.title);
        //onClick={()=>this.listItemClick(item.id)}
        return (
            <a href={tool.getdetailurl('/articledetail/'+item.id)} style={{color:"black"}}  target="_blank">
            <List.Item key={item.id} >
                 <Flex>
                     <Text className={st.title} style={{width:tool.getscreenwidth()-30}}>{item.title}</Text>
                </Flex>
             </List.Item>
             </a>
        );
    };

    onEndReached=()=> {
        //console.log("末尾"+this.state.pageCurrent);
        this.state.pageCurrent = this.state.pageCurrent+1;
        this.loadData();
        // this.setState({
        //     pageCurrent: this.state.pageCurrent + 1
        // }, () => {
        //     this.loadData();
        // });
    }

    render() {
        return (
            <Layout selectedTab={"Article"} {...this.props}>
                <Tabs tabs={this.state.category} onTabClick={this.tabClick}>
                </Tabs>
                <div style={{
                    width: '100%',
                    overflow: 'auto',
                    top: '6.5rem',
                    position: 'absolute',
                    //zIndex: 10,
                    bottom: '3rem',
                    }}>
                <SyList
                        initialListSize={20}
                        data={this.state.datalist}
                        renderRow={this.renderRow}
                        className="am-list"
                        pageSize={this.state.pageSize}
                        pageCurrent={this.state.pageCurrent}
                        onEndReached={this.onEndReached}
                        onEndReachedThreshold={5}
                    />
                    </div>
                <ActivityIndicator
                    toast
                    size="large"
                    text="数据加载中..."
                    animating={this.state.loading}
                />
            </Layout>
        );
    }
}
