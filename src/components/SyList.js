import React, {
    Component,
} from 'react'
import {ListView,Toast} from 'antd-mobile';

export default class SyList extends Component {
    constructor(props) {
        super(props);
        const dataSource = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        });
        this.state = {
            dataSource,
            height:((this.props.height===undefined)?"100%":this.props.height),
            overflow:((this.props.overflow===undefined)?"auto":this.props.overflow)
        };
       
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.data),
            height:((nextProps.height===undefined)?"100%":nextProps.height),
        });
    }
    render() {
        //console.log("刷新数据");
        return <ListView
            {...this.props}
            dataSource={this.state.dataSource}
            style={{height: this.state.height,overflow:this.state.overflow}}
            pageSize={this.props.pageSize}
            scrollRenderAheadDistance={500}
            onEndReached={this.onEndReached}
            onEndReachedThreshold={this.props.onEndReachedThreshold || 0.1}
        />
    }

    onEndReached = () => {
        if (this.props.data.length === 0) {
            return;
        }
        if (!this.props.pageSize) {
            return;
        }
        if (this.props.data.length % (this.props.pageSize * this.props.pageCurrent) !== 0) {
            if (this.props.pageCurrent > 1) {
                Toast.info('^_^没有更多数据啦~', 3, null, true);
            }
            return;
        }
        this.props.onEndReached && this.props.onEndReached()
    };
}
