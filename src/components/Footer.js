import React, {Component} from 'react';
import {routerRedux} from 'dva/router';
import {TabBar} from 'antd-mobile';
import tool from '../utils/tool';

export default class Footer extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: props.selectedTab ? props.selectedTab : 'Home',//选择TabBar.Item
        };
    }

    componentDidMount() {

    }

    //组件卸载
    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            selectedTab: nextProps.selectedTab ? nextProps.selectedTab : 'Home',//选择TabBar.Item
        });
    }

    render() {
        const {dispatch} = this.props;

        return (
            <div style={{position: 'fixed', width: '100%', bottom: 0, zIndex: 200}}>
                <TabBar
                    unselectedTintColor="#949494"
                    tintColor="#33A3F4"
                    barTintColor="#ddddde"
                    noRenderContent={true}
                    tabBarPosition={'bottom'}
                    prerenderingSiblingsNumber={0}
                >
                    <TabBar.Item
                        title="首页"
                        key="Home"
                        icon={<div style={{
                            width: '22px',
                            height: '22px',
                            background:`url(${require('../assets/imgs/home.png')}) center center /  21px 21px no-repeat`
                        }}
                        />
                        }
                        selectedIcon={<div style={{
                            width: '22px',
                            height: '22px',
                            background:`url(${require('../assets/imgs/home.png')}) center center /  21px 21px no-repeat`
                        }}
                        />
                        }
                        selected={this.state.selectedTab === 'Home'}
                        onPress={() => {
                            window.location=tool.getmainurl('/');
                            //dispatch(routerRedux.push('/'));
                        }}
                        data-seed="logId"
                    />
                    <TabBar.Item
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background:`url(${require('../assets/imgs/video.png')}) center center /  21px 21px no-repeat`
                            }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background:`url(${require('../assets/imgs/video.png')}) center center /  21px 21px no-repeat`
                            }}
                            />
                        }
                        title="视频"
                        key="Video"
                        selected={this.state.selectedTab === 'Video'}
                        onPress={() => {
                            window.location=tool.getmainurl('/video');
                            //dispatch(routerRedux.push('/video'));
                        }}
                    />
                     <TabBar.Item
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background:`url(${require('../assets/imgs/pic.png')}) center center /  21px 21px no-repeat`
                            }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background:`url(${require('../assets/imgs/pic.png')}) center center /  21px 21px no-repeat`
                            }}
                            />
                        }
                        title="图吧"
                        key="Picture"
                        selected={this.state.selectedTab === 'Picture'}
                        onPress={() => {
                            window.location=tool.getmainurl('/picture');
                            //dispatch(routerRedux.push('/picture'));
                        }}
                    />
                    <TabBar.Item
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background:`url(${require('../assets/imgs/article.png')}) center center /  21px 21px no-repeat`
                            }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background:`url(${require('../assets/imgs/article.png')}) center center /  21px 21px no-repeat`
                            }}
                            />
                        }
                        title="文章"
                        key="Article"
                        selected={this.state.selectedTab === 'Article'}
                        onPress={() => {
                            window.location=tool.getmainurl('/article');
                            //dispatch(routerRedux.push('/article'));
                        }}
                    />
                   
                    <TabBar.Item
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background:`url(${require('../assets/imgs/about.png')}) center center /  21px 21px no-repeat`
                            }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background:`url(${require('../assets/imgs/about.png')}) center center /  21px 21px no-repeat`
                            }}
                            />
                        }
                        title="关于"
                        key="About"
                        selected={this.state.selectedTab === 'About'}
                        onPress={() => {
                            window.location=tool.getmainurl('/about');
                            //dispatch(routerRedux.push('/about'));
                        }}
                    />
                </TabBar>
            </div>
        );
    }
}
