import React, {Component} from 'react';
import {Link,routerRedux} from 'dva/router';
import {NavBar,Icon} from 'antd-mobile';

export default class Header extends Component {
    componentDidMount() {

    }

    //组件卸载
    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {
    }

    backRender=()=>{
        if(this.props.Back)
        {
            return  <a style={{color:'white'}} href="javascript:" onClick={() => {
                window.close();//pc 打开新页面
                this.props.dispatch(routerRedux.goBack())// mobile 回到上一页
            }}><Icon type="left"/></a>;
        }
        return null;
    }

    render() {
        return (
            <NavBar
                mode="dark"
                icon={this.backRender()}
                leftContent={this.props.leftContent}
                rightContent={this.props.rightContent}
                style={{backgroundColor:'#fa2828',zIndex:5,top:0,width:'100%',height:'4rem',lineHeight:'4rem'}}
            >{this.props.centerContent}</NavBar>
        );
    }
}
