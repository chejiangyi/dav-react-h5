import React, {Component} from 'react';
import {Link} from 'dva/router';
import Header from '../components/Header';
import Footer from '../components/Footer';
import config from '../common/config';

export default class Layout extends Component {

    componentDidMount() {
    }

    //组件卸载
    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {
    }

    render() {
        return (
            <div>
                <Header {...this.props} centerContent={<span style={{color:'white'}}>{config.sitename}</span>}/>
                {this.props.children}
                <Footer {...this.props}/>
            </div>
        );
    }
}
